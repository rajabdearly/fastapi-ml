from fastapi import FastAPI
from tfModel import Model
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()
model = Model()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/")
async def greet():
    return {"message": "Welcome to our Movie Classifier API!"}


@app.get("/predict/{review}")
async def predict(review: str):
    predicted_proba = float(model.predict_proba(review))
    predicted_class = round(predicted_proba)
    return {
        "model": {
            "accuracy": model.accuracy,
            "version": model.version
        },
        "prediction": {
            "proba": predicted_proba,
            "class": predicted_class
        }
    }
