import tensorflow as tf
import numpy as np

class Model:
    def __init__(self):
        self.model = tf.keras.models.load_model('./myModel')
        self.version = 1.0
        self.accuracy = "86%"

    def predict_proba(self, review):
        return self.model.predict([review])[0][0]
